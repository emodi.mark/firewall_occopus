# Info
This is an Occopus template for [UFW](https://help.ubuntu.com/community/UFW) (uncomplicated firewall).
## Occopus install
[Occopus setup guide](http://www.lpds.sztaki.hu/occo/user/html/setup.html)
## Build requirements
Tested in ubuntu 16.04, but it should work with other operating systems.
## Attributes
There is a variables what you can set for your own preference. It is in the infrastructure definition file. 
infra-firewall.yaml
***
The rule variable is capable to set your UFW rules. You should give it from top to 
bottom, and leave a blank line after each rule or comment. 

For example: 
```bash
 rules: |

  # Deny incoming traffic with port 80

  ufw deny proto tcp from any to any port 80

  # BAN outgoing traffic with port 80

  ufw deny out to any port 80

  # Allow incoming traffic with port 443 

  ufw allow proto tcp from any to any port 443

  # Allow ssh traffic (port 22) 

  ufw allow ssh/tcp 
```
## Start
```bash
$ occopus-import nodes/node_definitions.yaml 
$ occopus-build infra-firewall.yaml
```
